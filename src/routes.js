import HomeComponent from './components/HomeComponent'
import AllCategories from './Views/AllCategories'
import AllItems from './Views/AllItems'

import AddCategories from './Views/AddCategories'
import AddItem from './Views/AddItem'


const routes = [
    { path: '/', component: HomeComponent},
  
    { path: '/AllItems', component: AllItems },
    { path: '/AllCategories', component: AllCategories },
    { path: '/AddCategories', component: AddCategories },
    { path: '/AddItem', component: AddItem },
]
export default routes;