import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './routes'
import vuetify from './plugins/vuetify'
import store from './store/store'


Vue.use(VueRouter)
const router = new VueRouter({
  routes
})

Vue.filter('changecurrency', (val) => {
  if (val.currency == 'Rupee') {
      return val.price
  } else if (val.currency == 'Dollar') {
      return Math.round(val.price * 76.31)
  }
})
Vue.directive('textColor', (el, binding) => {
  
  if (binding.value == "Inactive") {
      el.style.color = 'red'
  } else if (binding.value == "Active") {
      el.style.color = 'green'
  } else {
       el.style.color = "black "
  }
})



Vue.config.productionTip = false

new Vue({
   store,
  router ,
  vuetify,
  render: h => h(App)
}).$mount('#app')