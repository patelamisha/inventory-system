import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categoryShow: false,
        itemShow: true,
        itemsHeaders: [
            { text: "Id", align: "start", sortable: true, value: "id" },
            { text: "Name", value: "name" },
            { text: "Category", value: "category" },
            { text: "Description", value: "description",  },
            { text: "Price", value: "price" },
            { text: "Status", value: "status" },
            { text: "Actions", value: "actions",  },
        ],
        itemsAllData: [{
            id: 1,
            name: "Mobile",
            category: 'Electronics',
            description: "Mi 4A",
            price: 15,
            currency: 'Dollar',
            status: "Active",
        },
        {
            id: 2,
            name: "shoes",
            category: 'Fashion',
            description: "Adidas",
            price: 1000,
            currency: 'Rupee',
            status: "Inactive",
        },
    ],
    itemAddData: {
        id: null,
        name: "",
        category: '',
        description: "",
        price: null,
        currency: '',
        status: "",
    },
        categoriesHeaders: [
            { text: "Id", align: "start", sortable: true, value: "id" },
            { text: "Name", value: "name" },
            { text: "Description", value: "description", sortable: false },
            { text: "Status", value: "status" },
            { text: "Actions", value: "actions", sortable: false },
        ],
        categoriesAllData: [{
            id: 2,
            name: "clothes",
            description: "crop top",
            status: "Active",
        },
        {
            id: 1,
            name: "food",
            description: "dhosa",
            status: "Inactive",
        },
        {
          id: 3,
          name: "mobile",
          description: "Mi",
          status: "Inactive",
      },
    ],
    categoryAddData: {
        id: null,
        name: "",
        description: "",
        status: "",
    },

        
        
      
       
        CategoryItems: [
            "Electronics",
            "Fashion",
            "Food"
            
            
        ],
    CurrencyItems: ["Rupee", "Dollar"],
    },

    mutations: {
        
        deleteItemData(state, item) {
            let index = state.itemsAllData.indexOf(item)
            state.itemsAllData.splice(index, 1)
        },
        deleteCategoryData(state, item) {
            let index = state.categoriesAllData.indexOf(item)
            state.categoriesAllData.splice(index, 1)
        },
       
    },
    actions: {
      
        deleteItemData({ commit }, item) {
            commit('deleteItemData', item)
        },
        deleteCategoryData({ commit }, item) {
            commit('deleteCategoryData', item)
        },
       
    }
});